import streamlit as st
from streamlit_folium import folium_static
import pandas as pd
import json
from traffic_risk_assessment.dashboard import routing, maps, shared
from traffic_risk_assessment import CONFIG


@st.cache
def load_data():
    shared.setup()
    return routing.incidents_from_json(CONFIG.HISTORICAL_INCIDENTS_PATH)


def main():
    st.write("## ⛬ Historical Analysis")
    with st.spinner("Loading data"):
        incident_data = load_data()
    # st.write(incident_data)
    st.write("### Historical traffic incidents:")
    st.write(
        "#### The map shows routes between your origins and destinations and clustered historical traffic incidents per hour of day.\n"
    )
    with st.spinner("Generating Map"):
        m = maps.generate_time_heatmap(incident_data)
        m = maps.map_geojson_route(st.session_state["HERE_ROUTES"], add_to_map=m)
    folium_static(m)

    # st.dataframe(origin_destination)


if __name__ == "__main__":
    main()
