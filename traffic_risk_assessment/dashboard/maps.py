import typing as t
import pandas as pd
import geopandas as gpd
import numpy as np
from here_location_services.responses import RoutingResponse
import folium
import pydeck
import io
import json
from folium.plugins import HeatMap, HeatMapWithTime
from traffic_risk_assessment import CONFIG

GREEN_RGB = [0, 255, 0]
RED_RGB = [255, 0, 0]


def map_geojson_route(
    geojson: t.Union[dict, gpd.GeoDataFrame], add_to_map=None
) -> folium.Map:
    """Build a map from a RoutingResponse"""
    if isinstance(geojson, gpd.GeoDataFrame):
        geojson = json.loads(geojson.to_json())

    def colors():
        while True:
            for x in ["red", "blue", "darkgreen", "darkpurple", "gray", "black"]:
                yield x

    color_generator = colors()

    geo_layer = folium.GeoJson(
        data=geojson,
        style_function=lambda x: {
            "lineWidth": 5,
            "color": next(color_generator),
            "opacity": 0.5,
        },
    )

    origins = [
        list(reversed(feature["geometry"]["coordinates"][0][:2]))
        for feature in geojson["features"]
    ]
    destinations = [
        list(reversed(feature["geometry"]["coordinates"][-1][:2]))
        for feature in geojson["features"]
    ]
    if add_to_map:
        m = add_to_map
    else:
        m = folium.Map([50, 11], tiles="cartodbpositron", zoom_start=8)
    origin_markers = [
        folium.Marker(
            origin,
            icon=folium.Icon(color="green", icon="play", prefix="fa"),
            tooltip="Origin",
        )
        for origin in origins
    ]
    dest_markers = [
        folium.Marker(
            destination,
            icon=folium.Icon(color="red", icon="flag", prefix="fa"),
            tooltip="Destination",
        )
        for destination in destinations
    ]
    geo_layer.add_to(m)
    [origin_marker.add_to(m) for origin_marker in origin_markers]
    [dest_marker.add_to(m) for dest_marker in dest_markers]
    return m


def scatter_origin_destination(origin, destination):
    origin = origin.rename(columns={"origin": "name"})
    destination = destination.rename(columns={"destination": "name"})

    return pydeck.Deck(
        map_style=pydeck.map_styles.MAPBOX_ROAD,
        initial_view_state=pydeck.ViewState(
            latitude=50,
            longitude=11,
            zoom=5,
        ),
        api_keys={"mapbox": CONFIG.MAPBOX_API_TOKEN},
        layers=[
            pydeck.Layer(
                "ScatterplotLayer",
                data=origin,
                get_position=["lon", "lat"],
                get_fill_color=GREEN_RGB,
                get_line_color=GREEN_RGB,
                get_radius=5000,
                pickable=True,
                opacity=0.8,
                stroked=True,
                filled=True,
            ),
            pydeck.Layer(
                "ScatterplotLayer",
                data=destination,
                get_position=["lon", "lat"],
                get_fill_color=RED_RGB,
                get_line_color=RED_RGB,
                get_radius=5000,
                pickable=True,
                opacity=0.8,
                stroked=True,
                filled=True,
            ),
        ],
        tooltip={"html": "{name}"},
    )


def arc_origin_destination(df):
    TOOLTIP_TEXT = {
        "html": "{origin} (green) -> {destination} (red) | Distance: {distance_x} km"
    }

    return pydeck.Deck(
        map_style=pydeck.map_styles.MAPBOX_ROAD,
        initial_view_state=pydeck.ViewState(
            latitude=50,
            longitude=11,
            zoom=5,
        ),
        api_keys={"mapbox": CONFIG.MAPBOX_API_TOKEN},
        layers=[
            pydeck.Layer(
                "ArcLayer",
                data=df,
                get_source_position=["lng_x", "lat_x"],
                get_target_position=["lng_y", "lat_y"],
                get_source_color=GREEN_RGB,
                get_target_color=RED_RGB,
                get_width=5,
                pickable=True,
                auto_highlight=True,
                get_tilt=15,
            )
        ],
        tooltip=TOOLTIP_TEXT,
    )


def great_circle_origin_destination(df):
    TOOLTIP_TEXT = {
        "html": "{origin} (green) -> {destination} (red) | Distance: {distance_x} km"
    }

    return pydeck.Deck(
        map_style=pydeck.map_styles.MAPBOX_ROAD,
        initial_view_state=pydeck.ViewState(
            latitude=50,
            longitude=11,
            zoom=5,
        ),
        api_keys={"mapbox": CONFIG.MAPBOX_API_TOKEN},
        layers=[
            pydeck.Layer(
                "GreatCircleLayer",
                data=df,
                get_source_position=["lng_x", "lat_x"],
                get_target_position=["lng_y", "lat_y"],
                get_source_color=GREEN_RGB,
                get_target_color=RED_RGB,
                get_stroke_width=12,
                pickable=True,
                auto_highlight=True,
            )
        ],
        tooltip=TOOLTIP_TEXT,
    )


def generate_heatmap(df, add_to_map=None):

    heatmap = HeatMap(df[["lat", "lon"]]).add_to(folium.FeatureGroup(name="Heatmap"))
    if add_to_map:
        m = add_to_map
    else:
        m = folium.Map([50, 11], tiles="cartodbpositron", zoom_start=8)
    heatmap.add_to(m)
    return m


def generate_time_heatmap(
    df,
    lat_col="from_latitude",
    lon_col="from_longitude",
    time_col="start_time",
    interval="1H",
    add_to_map=None,
):
    df = df.rename(columns={lat_col: "lat", lon_col: "lon"})
    time = pd.to_datetime(df[time_col])
    # df = df.set_index(time_col)
    data = []
    time_index = []
    for key, d in df.groupby(time.dt.hour):
        data.append([[row["lat"], row["lon"]] for _, row in d.iterrows()])
        time_index.append(f"Hour of day: {str(key)}")
    # date_strings = [str(d) for d in time_index]
    heatmap = HeatMapWithTime(
        data, index=time_index, radius=15, auto_play=True, position="bottomright"
    )
    if add_to_map:
        m = add_to_map
    else:
        m = folium.Map([50, 11], tiles="cartodbpositron", zoom_start=8)
    heatmap.add_to(m)
    return m
