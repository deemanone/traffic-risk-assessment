import io
import pathlib
import typing as t
import datetime
import pandas as pd
import geopandas as gpd
import json
from dataprep.clean import clean_headers, clean_df
from here_location_services import LS
from here_location_services.config.routing_config import ROUTING_RETURN

import herepy
from shapely.geometry import LineString, Polygon
import httpx
import diskcache as dc
import time
import concurrent.futures
from traffic_risk_assessment import CONFIG
import logging

logger = logging.getLogger(__name__)
cache = dc.Cache(CONFIG.CACHE_DIR)

HERE_LOCATION_SERVICE = LS(api_key=CONFIG.HERE_API_KEY)
HERE_TRAFFIC_API = herepy.TrafficApi(CONFIG.HERE_API_KEY)


@cache.memoize(tag="HERE")
def request_here_truck_route(
    origin: t.Tuple[float, float],
    destination: t.Tuple[float, float],
    routing_mode="fast",
    departure_time: datetime.datetime = None,
) -> pd.DataFrame:
    """Requests a route from the here.com routing api and returns the result as a GeoDataFrame
    :origin: and :destination: need to be in (lat,lng) format"""
    logger.info(f"Requesting route for {origin}-{destination}:{departure_time}")

    response = HERE_LOCATION_SERVICE.truck_route(
        origin=origin,
        destination=destination,
        return_results=[
            ROUTING_RETURN.polyline,
            ROUTING_RETURN.elevation,
            ROUTING_RETURN.instructions,
            ROUTING_RETURN.actions,
            ROUTING_RETURN.incidents,
            ROUTING_RETURN.summary,
            ROUTING_RETURN.travelSummary,
        ],
        routing_mode=routing_mode,
        alternatives=6,
        departure_time=departure_time,
    )
    return response.to_geojson()


@cache.memoize(tag="MAPBOX")
def request_mapbox_route(
    origin: t.Tuple[float, float], destination: t.Tuple[float, float]
):
    """Request a route from the mapbox api. :origin: and :destination: need to be in lon,lat format."""

    url = f"https://api.mapbox.com/directions/v5/mapbox/driving-traffic/{origin[0]},{origin[1]};{destination[0]},{destination[1]}?alternatives=true&geometries=geojson&steps=true&access_token={CONFIG.MAPBOX_API_TOKEN}"
    resp = httpx.get(url).json()

    return resp


def geojson_to_geodataframe(geojson):
    df = gpd.GeoDataFrame.from_features(geojson.get("features"), crs="EPSG:4326")
    summary = pd.json_normalize(df.summary)
    departure = pd.json_normalize(df.departure).rename(
        columns=lambda x: f"departure.{x}"
    )
    transport = pd.json_normalize(df.transport)
    arrival = pd.json_normalize(df.arrival).rename(columns=lambda x: f"arrival.{x}")
    actions = df.actions
    df = df.drop(
        columns=[
            "actions",
            "travelSummary",
            "polyline",
            "language",
            "id",
            "summary",
            "transport",
            "arrival",
            "departure",
        ]
    )
    df = pd.concat([df, summary, departure, arrival, transport], axis=1)
    df = clean_headers(df, report=False)
    df = pd.concat([df, actions], axis=1)
    return df


def request_bunch_of_here_routes(
    origin_destination_list: t.Iterable,
) -> t.List[gpd.GeoDataFrame]:
    def request_one(origin, destination):
        try:
            return request_here_truck_route(origin, destination)
        except Exception as e:
            time.sleep(1)
            return request_here_truck_route(origin, destination)

    with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
        future_to_request = {
            executor.submit(request_one, origin, destination): (origin, destination)
            for origin, destination in origin_destination_list
        }
        for future in concurrent.futures.as_completed(future_to_request):
            org, dest = future_to_request[future]
            gdf = geojson_to_geodataframe(future.result())
            gdf["origin_lat"], gdf["origin_lon"] = org
            gdf["destination_lat"], gdf["destination_lon"] = dest
            yield gdf


def request_bunch_of_mapbox_routes(
    origin_destination_list: t.Iterable,
) -> t.List[pd.DataFrame]:
    def request_one(origin, destination):
        try:
            return request_mapbox_route(origin, destination)
        except Exception as e:
            time.sleep(1)
            return request_mapbox_route(origin, destination)

    with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
        future_to_request = {
            executor.submit(request_one, origin, destination): (origin, destination)
            for origin, destination in origin_destination_list
        }
        for future in concurrent.futures.as_completed(future_to_request):
            org, dest = future_to_request[future]
            mb_df = pd.json_normalize(future.result().get("routes"))
            geometry = mb_df["geometry.coordinates"].apply(LineString)
            mb_df["geometry"] = geometry
            mb_df = mb_df.drop(columns=["geometry.coordinates", "geometry.type"])
            gdf = gpd.GeoDataFrame(mb_df, geometry="geometry", crs="EPSG:4326")
            gdf["origin_lat"], gdf["origin_lon"] = list(reversed(org))
            gdf["destination_lat"], gdf["destination_lon"] = list(reversed(dest))
            yield gdf


def load_routes(raw_data):
    origins, destinations = routes_from_csv(raw_data)
    origins = zip(origins.lat, origins.lon)
    destinations = zip(destinations.lat, destinations.lon)
    here_bunch = list(zip(origins, destinations))
    here_routes = list(request_bunch_of_here_routes(here_bunch))
    here_routes = pd.concat(here_routes)
    mapbox_bunch = [reversed(x) for x in here_bunch]
    mapbox_routes = list(request_bunch_of_mapbox_routes(mapbox_bunch))
    # mapbox_routes = pd.concat(mapbox_routes)
    return here_routes, mapbox_routes


def get_congestion_geometry(df: pd.DataFrame) -> pd.DataFrame:
    """Adds a geometry column to the dataframe"""
    df["geometry"] = df.apply(
        lambda x: LineString(
            [(x.from_longitude, x.from_latitude), (x.to_longitude, x.to_latitude)]
        ),
        axis=1,
    )
    return df


def clean_congestion_data(df: pd.DataFrame) -> pd.DataFrame:
    """Cleans the Congestion data"""
    df = df.pipe(clean_headers, report=False).rename(
        columns={
            "location_geoloc_origin_latitude": "from_latitude",
            "location_geoloc_origin_longitude": "from_longitude",
        }
    )
    df.traffic_item_description = pd.json_normalize(
        df.traffic_item_description.apply(lambda x: x[0])
    )["value"]
    df = (
        pd.json_normalize(df.location_geoloc_to.apply(lambda x: x[0]))
        .rename(columns={"LATITUDE": "to_latitude", "LONGITUDE": "to_longitude"})
        .merge(df, left_index=True, right_index=True)
        .drop(columns=["location_geoloc_to"])
    )
    df = df[
        [
            "start_time",
            "end_time",
            "entry_time",
            "verified",
            "from_latitude",
            "from_longitude",
            "to_latitude",
            "to_longitude",
            "location_length",
            "traffic_item_detail_incident_congestion_incident_congestion_type_desc",
            "traffic_item_detail_incident_congestion_incident_congestion_factor",
            "traffic_item_description",
            "traffic_item_detail_road_closed",
            "traffic_item_detail_incident_response_vehicles",
            "criticality_description",
            "abbreviation_description",
        ]
    ]
    df = clean_df(df, report=False)[1]
    return df


def incidents_from_json(path: pathlib.Path) -> pd.DataFrame:
    """Reads a folder of congestion data in json format and return it as a Dataframe"""
    congestions = []
    for f in path.glob("*.json"):
        with open(f, "rb") as file_:
            js = json.load(file_)
            if "TRAFFIC_ITEMS" in js.keys():
                for traffic_item in js.get("TRAFFIC_ITEMS").get("TRAFFIC_ITEM"):
                    congestions.append(traffic_item)
    df = pd.json_normalize(congestions)
    return df.pipe(clean_congestion_data)


def routes_from_csv(
    csv: t.Union[pathlib.Path, io.BytesIO, pd.DataFrame]
) -> t.Tuple[pd.DataFrame, pd.DataFrame]:
    """Reads a csv with given origin destination pairs. CSV has to be structured like the example csv in the data folder"""
    if not isinstance(csv, pd.DataFrame):
        df = pd.read_csv(csv)
    else:
        df = csv

    c1_cols = [col for col in df.columns if "city1" in col]
    c2_cols = [col for col in df.columns if "city2" in col]
    c1_cols.append("distance")
    c2_cols.append("distance")

    city1_df = df[c1_cols]
    city2_df = df[c2_cols]

    c1_coords = get_coords_from_string_col(city1_df.city1coords)
    c2_coords = get_coords_from_string_col(city2_df.city2coords)

    city1_df = city1_df.merge(c1_coords, left_index=True, right_index=True)
    city2_df = city2_df.merge(c2_coords, left_index=True, right_index=True)

    city1_df = city1_df.drop("city1coords", axis=1).rename(columns={"city1": "origin"})
    city2_df = city2_df.drop("city2coords", axis=1).rename(
        columns={"city2": "destination"}
    )
    return city1_df, city2_df


def get_coords_from_string_col(series: pd.Series) -> pd.DataFrame:
    coords = series.str.replace("'", '"').apply(json.loads)
    coords = pd.json_normalize(coords)
    coords.lat = pd.to_numeric(coords.lat)
    coords.lon = pd.to_numeric(coords.lon)

    return coords


def corridor_from_linestring(
    linestring: LineString, corridor_width: float = 0.01
) -> Polygon:
    """Return a corridor sourrounding a linestring given a width.
    Assumes linestring uses EPSG:4326 projection. 0.01° ~ 1.11km (see https://www.usna.edu/Users/oceano/pguth/md_help/html/approx_equivalents.htm)"""
    return linestring.buffer(corridor_width)
